# account-manager-svc

Service to get balance and transfer between accounts

## Set up

This project requires JDK 17, and is tested to run on IntelliJ IDEA.

## Running

To run the service, use

```sh
./gradlew bootRun
```

Additionally, there is a make command to run the service

```sh
make run
```


This will populate database with test data. If you wish to run without test data, then run

```shell
make no-populate-run
```

To run tests, run `make test`

## API Documentation

This project is configured to generate API documentation with OpenAPI specification. To get this, run the service and go to the path `/api-docs`, or to see in swagger ui, go to `/swagger-ui.html`

Currently, there is 2 API endpoints:

---

### Get balance

```shell
GET /account/{id}/balance
```
where `{id}` is the account id that requested the balance. will return JSON file with format

```
{
  "accountId": string,
  "balance": string
}
```

### Transfer
```shell
POST /account/{id}/transfer
```
where `{id}` is the initiator of transfer request. with JSON body

```
{
  "recipientId": string,
  "amount": string
}
```

and returns a JSON response indicating if the transfer is successful.
