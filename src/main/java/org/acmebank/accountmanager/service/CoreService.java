package org.acmebank.accountmanager.service;

import org.acmebank.accountmanager.exception.AccountNotFoundException;
import org.acmebank.accountmanager.exception.TransferFailedException;
import org.acmebank.accountmanager.model.Account;

public interface CoreService {
    public Account getAccountById(Integer accountId) throws AccountNotFoundException;
    public void transfer(Integer initiatorId, Integer recipientId, Long amount) throws TransferFailedException;
    public void saveAccounts(Iterable<Account> accounts);
}
