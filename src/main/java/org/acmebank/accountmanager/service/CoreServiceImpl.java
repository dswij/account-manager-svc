package org.acmebank.accountmanager.service;

import org.acmebank.accountmanager.exception.AccountNotFoundException;
import org.acmebank.accountmanager.exception.TransferFailedException;
import org.acmebank.accountmanager.exception.TransferFailureReason;
import org.acmebank.accountmanager.model.Account;
import org.acmebank.accountmanager.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CoreServiceImpl implements CoreService {
    @Autowired
    private AccountRepository accountRepository;

    public Long getAccountBalance(Integer accountId) throws AccountNotFoundException {
        return getAccountById(accountId).getBalance();
    }

    public Account getAccountById(Integer accountId) throws AccountNotFoundException {
        return accountRepository.findById(accountId).orElseThrow(() -> new AccountNotFoundException(accountId));
    }

    public void transfer(Integer initiatorId, Integer recipientId, Long amount) throws AccountNotFoundException, TransferFailedException {
        Account initiator = accountRepository.findById(initiatorId).orElseThrow(() -> new AccountNotFoundException(initiatorId));
        Account recipient = accountRepository.findById(recipientId).orElseThrow(() -> new AccountNotFoundException(recipientId));

        Long initiatorBalance = initiator.getBalance();
        if (initiatorBalance <= amount) {
            throw new TransferFailedException("Not enough balance", TransferFailureReason.NOT_ENOUGH_BALANCE);
        }

        Long recipientBalance = recipient.getBalance();
        initiator.setBalance(initiatorBalance - amount);
        recipient.setBalance(recipientBalance + amount);
    }

    public void saveAccounts(Iterable<Account> accounts){
        accountRepository.saveAll(accounts);
    }

}
