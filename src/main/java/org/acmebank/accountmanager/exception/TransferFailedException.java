package org.acmebank.accountmanager.exception;

public class TransferFailedException extends RuntimeException {
    private final TransferFailureReason reason;

    public TransferFailedException(String msg, TransferFailureReason reason) {
        super(msg);
        this.reason = reason;
    }

    public TransferFailureReason getReason() {
        return this.reason;
    }
}
