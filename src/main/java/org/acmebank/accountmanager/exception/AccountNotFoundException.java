package org.acmebank.accountmanager.exception;

public class AccountNotFoundException extends RuntimeException {
    public AccountNotFoundException(Integer id) {
        super("Unable to find account with id: " + id);
    }
}
