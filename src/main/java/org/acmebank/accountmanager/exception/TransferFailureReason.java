package org.acmebank.accountmanager.exception;

public enum TransferFailureReason {
    NOT_ENOUGH_BALANCE,
    SERVER_ERROR
}
