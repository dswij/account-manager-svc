package org.acmebank.accountmanager.helper;

public class TransferRequest {
    public Integer recipientId;
    public Long amount;

    public TransferRequest(Integer recipientId, Long amount) {
        this.recipientId = recipientId;
        this.amount = amount;
    }
}
