package org.acmebank.accountmanager.controller;

import org.acmebank.accountmanager.exception.AccountNotFoundException;
import org.acmebank.accountmanager.exception.TransferFailedException;
import org.acmebank.accountmanager.exception.TransferFailureReason;
import org.acmebank.accountmanager.helper.TransferRequest;
import org.acmebank.accountmanager.repository.AccountRepository;
import org.acmebank.accountmanager.service.CoreServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.util.HashMap;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private CoreServiceImpl coreService;

    @GetMapping("/{id}/balance")
    public ResponseEntity<Object> balance(@PathVariable Integer id) {
        Long balance = coreService.getAccountBalance(id);

        HashMap<String, Object> response = new HashMap<>();
        response.put("accountId", id.toString());
        response.put("balance", balance);

        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @PostMapping(path = "/{id}/transfer",  consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> transfer(@PathVariable Integer id, @RequestBody TransferRequest body) {
        coreService.transfer(id, body.recipientId, body.amount);

        HashMap<String, Object> response = new HashMap<>();
        response.put("transactionStatus", "success");
        response.put("message", "transfer completed successfully");

        return new ResponseEntity<Object>(response, HttpStatus.OK);
    }

    @ExceptionHandler({ AccountNotFoundException.class })
    public ResponseEntity<Object> handleAccountNotFoundException(AccountNotFoundException exception, WebRequest _request) {
        HashMap<String, Object> response = new HashMap<>();
        String msg = String.format(exception.getMessage());
        response.put("message", msg);

        return new ResponseEntity<Object>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({ TransferFailedException.class })
    public ResponseEntity<Object> handleTransferFailedException(TransferFailedException exception, WebRequest _request) {
        HttpStatus status;
        if (exception.getReason() == TransferFailureReason.NOT_ENOUGH_BALANCE) {
            status = HttpStatus.BAD_REQUEST;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        HashMap<String, Object> response = new HashMap<>();
        String msg = String.format(exception.getMessage());
        response.put("message", msg);

        return new ResponseEntity<Object>(response, status);
    }
}