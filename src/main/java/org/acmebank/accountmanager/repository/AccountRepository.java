package org.acmebank.accountmanager.repository;

import org.acmebank.accountmanager.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, Integer> {}
