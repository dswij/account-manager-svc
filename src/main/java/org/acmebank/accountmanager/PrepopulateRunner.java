package org.acmebank.accountmanager;

import org.acmebank.accountmanager.repository.AccountRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;


@Component
public class PrepopulateRunner implements ApplicationRunner {
    /** Class used to prepopulate "Accounts" table
     *
     */
    private static final Logger logger = LoggerFactory.getLogger(PrepopulateRunner.class);

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private DataSource dataSource;

    @Value("${prepopulate:enabled}")
    private String prepopulate;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (prepopulate.equals("disabled") || accountRepository.count() > 0) {
            return;
        }

        logger.info("Prepopulating database...");
        ClassPathResource scriptResource = new ClassPathResource("_prepopulate.sql");
        ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", scriptResource);
        resourceDatabasePopulator.execute(dataSource);
    }

}
