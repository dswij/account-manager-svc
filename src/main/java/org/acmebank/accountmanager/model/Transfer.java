package org.acmebank.accountmanager.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.acmebank.accountmanager.model.Account;

import javax.persistence.*;

@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Entity
@Table(name = "transfers")
public class Transfer {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @OneToOne
    private Account initiator;

    @OneToOne
    private Account recipient;

    @Column(name = "Amount")
    private Long amount;

    public Account getInitiator(Account initiator) {
        return initiator;
    }

    public void setInitiator(Account initiator) {
        this.initiator = initiator;
    }

    public Account getRecipient() {
        return recipient;
    }

    public void setRecipient(Account recipient) {
        this.recipient = recipient;
    }

    public Long getAmount() {
        return amount;
    }

    public void setAmount(Long amount) {
        this.amount = amount;
    }
}


