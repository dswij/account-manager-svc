package org.acmebank.accountmanager.service;

import org.acmebank.accountmanager.exception.TransferFailedException;
import org.acmebank.accountmanager.exception.TransferFailureReason;
import org.acmebank.accountmanager.model.Account;
import org.acmebank.accountmanager.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@WebMvcTest(CoreServiceImpl.class)
@ExtendWith(MockitoExtension.class)
public class CoreServiceTest {

    @MockBean
    private AccountRepository accountRepository;

    @InjectMocks
    private CoreServiceImpl coreService;

    @Test
    public void whenTransferItTransferBalance() {
        Account initiatorAccount = new Account(12345678, 1000L);
        Account recipientAccount = new Account(88888888, 1000L);
        when(accountRepository.findById(initiatorAccount.getId())).thenReturn(Optional.of(initiatorAccount));
        when(accountRepository.findById(recipientAccount.getId())).thenReturn(Optional.of(recipientAccount));

        coreService.transfer(initiatorAccount.getId(), recipientAccount.getId(), 500L);

        assertThat(initiatorAccount.getBalance()).isEqualTo(500L);
        assertThat(recipientAccount.getBalance()).isEqualTo(1500L);
    }

    @Test
    public void doesNotTransferWhenNotEnoughBalance() {
        Account initiatorAccount = new Account(12345678, 1000L);
        Account recipientAccount = new Account(88888888, 1000L);
        when(accountRepository.findById(initiatorAccount.getId())).thenReturn(Optional.of(initiatorAccount));
        when(accountRepository.findById(recipientAccount.getId())).thenReturn(Optional.of(recipientAccount));

        TransferFailedException exception = assertThrows(
                TransferFailedException.class,
                () -> {
                    coreService.transfer(initiatorAccount.getId(), recipientAccount.getId(), 1001L);
                }
        );

        assertThat(exception.getReason()).isEqualTo(TransferFailureReason.NOT_ENOUGH_BALANCE);
        assertThat(exception.getMessage()).isEqualTo("Not enough balance");
    }
}
