package org.acmebank.accountmanager;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.acmebank.accountmanager.helper.TransferRequest;
import org.acmebank.accountmanager.model.Account;
import org.acmebank.accountmanager.repository.AccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.web.servlet.MockMvc;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class AccountManagerApplicationTests extends AbstractTransactionalJUnit4SpringContextTests {

    @LocalServerPort
    private int port;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    @Sql(scripts = {"data-test.sql"})
    public void testGetBalance() throws Exception {
        Integer id = 12345678;
        String url = String.format("http://localhost:%d/account/%s/balance", port, id);

        mockMvc.perform(get(url)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accountId").value(id))
                .andExpect(jsonPath("$.balance").value(1000000));
    }

    @Test
    @Sql(scripts = {"data-test.sql"})
    public void testTransferBalance() throws Exception {
        Integer initiatorId = 12345678;
        Integer recipientId = 88888888;
        Long amount = 500000L;
        String url = String.format("http://localhost:%d/account/%d/transfer", port, initiatorId);

        mockMvc.perform(post(url)
                        .accept(MediaType.APPLICATION_JSON)
                        .content((new ObjectMapper()).writeValueAsString(new TransferRequest(recipientId, amount)))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionStatus").value("success"))
                .andExpect(jsonPath("$.message").value("transfer completed successfully"));

        Account initiator = accountRepository.getById(initiatorId);
        assertThat(initiator.getBalance()).isEqualTo(500000L);

        Account recipient = accountRepository.getById(recipientId);
        assertThat(recipient.getBalance()).isEqualTo(1500000L);
    }

}
