package org.acmebank.accountmanager.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.acmebank.accountmanager.helper.TransferRequest;
import org.acmebank.accountmanager.model.Account;
import org.acmebank.accountmanager.repository.AccountRepository;
import org.acmebank.accountmanager.service.CoreServiceImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AccountController.class)
public class AccountControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CoreServiceImpl coreService;

    @MockBean
    private AccountRepository accountRepository;

    @Test
    public void balanceShouldReturnBalance() throws Exception {
        Integer accountId = 123456678;
        Long balance = 100L;
        String url = String.format("/account/%d/balance", accountId);

        Account mockAccount = new Account(accountId, balance);
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(mockAccount));
        when(coreService.getAccountBalance(accountId)).thenReturn(balance);

        mockMvc.perform(get(url))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.balance").value(String.valueOf(balance)))
                .andExpect(jsonPath("$.accountId").value(String.valueOf(accountId)));
    }

    @Test
    public void transferShouldReturnStatusAndMessage() throws Exception {
        Integer accountId = 123456678;
        Integer recipientId = 88888888;
        Long balance = 100L;
        String url = String.format("/account/%d/transfer", accountId);

        Account mockAccount = new Account(accountId, balance);
        when(accountRepository.findById(accountId)).thenReturn(Optional.of(mockAccount));
        when(coreService.getAccountBalance(accountId)).thenReturn(balance);

        mockMvc.perform(MockMvcRequestBuilders.post(url)
                        .content(new ObjectMapper().writeValueAsString(new TransferRequest(recipientId, 5L)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON)
                )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.transactionStatus").value("success"))
                .andExpect(jsonPath("$.message").value("transfer completed successfully"));
    }
}
